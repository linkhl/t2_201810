package model.vo;

import sun.util.calendar.LocalGregorianCalendar.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> 
{
	/**
	 * dropoff_census_tract
	 */
	private double dropoff_census_tract;

	/**
	 * dropoff_centroid_latitude
	 */
	private String dropoff_centroid_latitude;

	/**
	 * dropoff_centroid_location 
	 */
	private Object dropoff_centroid_location;

	/**
	 * dropoff_centroid_longitude
	 */
	private String dropoff_centroid_longitude;

	/**
	 * dropoff_community_area
	 */
	private short dropoff_community_area;

	/**
	 * extras.
	 */
	private double extras;

	/**
	 * fare.
	 */
	private double fare;

	/**
	 * payment_type
	 */
	private String payment_type;

	/**
	 * pickup_census_tract
	 */
	private double pickup_census_tract;

	/**
	 * pickup_centroid_latitude
	 */
	private String pickup_centroid_latitude;

	/**
	 * pickup_centroid_location
	 */
	private Object pickup_centroid_location;

	/**
	 * pickup_centroid_longitude
	 */
	private String pickup_centroid_longitude;

	/**
	 * pickup_community_area
	 */
	private short pickup_community_area;

	/**
	 * taxi ID
	 */
	private String taxi_id;

	/**
	 * tips
	 */
	private int tips;

	/**
	 * tolls.
	 */
	private int tolls;

	/**
	 * trip_end_timestamp
	 */
	private Date trip_end_timestamp;

	/**
	 * Trip Id
	 */
	private String trip_id;

	/**
	 * trip in Miles
	 */
	private double trip_miles;

	/**
	 * TripInSeconds.
	 */
	private int trip_seconds;

	/**
	 * trip_start_timestamp
	 */
	private Date trip_start_timestamp;

	/**
	 * TripTotal.
	 */
	private double trip_total;

	// double dropoff_census_tract - double dropoff_centroid_latitude - Object dropoff_centroid_location 
	// double dropoff_centroid_longitude - short dropoff_community_area 
	// double extras - double fare - String payment_type 
	// double pickup_census_tract - double pickup_centroid_latitude - Object pickup_centroid_location
	// double pickup_centroid_longitude - short pickup_community_area 
	// String taxi_id - int tips - int tolls
	// Date trip_end_timestamp - String trip_id - int trip_seconds - double trip_miles 
	// Date trip_start_timestamp - double trip_total
	/**
	 * Constructor of the class service.
	 * @param pTripId trip ID.
	 * @param pTaxiId taxi Id
	 * @param pSeconds time in seconds of the service.
	 * @param pTripM trip Miles.
	 * @param pTripT trip total.
	 */
	public Service(short dropoff_community_area , String taxi_id , String trip_id , int trip_seconds , double trip_miles , double trip_total)
	{
//		this.dropoff_census_tract		= dropoff_census_tract;
//		this.dropoff_centroid_latitude	= dropoff_centroid_latitude;
//		this.dropoff_centroid_location	= dropoff_centroid_location;
//		this.dropoff_centroid_longitude = dropoff_centroid_longitude; 
		this.dropoff_community_area		= dropoff_community_area; 
//		this.extras 					= extras;
//		this.fare						= fare;
//		this.payment_type				= payment_type; 
//		this.pickup_census_tract		= pickup_census_tract;
//		this.pickup_centroid_latitude	= pickup_centroid_latitude;
//		this.pickup_centroid_location	= pickup_centroid_location;
//		this.pickup_centroid_longitude	= pickup_centroid_longitude;
//		this.pickup_community_area		= pickup_community_area;
		this.taxi_id 					= taxi_id;
//		this.tips						= tips;
//		this.tolls						= tolls;
//		this.trip_end_timestamp 		= trip_end_timestamp;
		this.trip_id					= trip_id;
		this.trip_seconds				= trip_seconds;
		this.trip_miles					= trip_miles;
//		this.trip_start_timestamp		= trip_start_timestamp;
		this.trip_total					= trip_total;
	}

	/** PARAMETROS PARA EL PROYECTO
	double dropoff_census_tract , String dropoff_centroid_latitude , Object dropoff_centroid_location , 
	String dropoff_centroid_longitude , short dropoff_community_area , 
	double extras , double fare , String payment_type , 
	double pickup_census_tract , String pickup_centroid_latitude , Object pickup_centroid_location ,
	String pickup_centroid_longitude , short pickup_community_area , 
	String taxi_id , int tips , int tolls ,
	Date trip_end_timestamp , String trip_id , int trip_seconds , double trip_miles , 
	Date trip_start_timestamp , double trip_total
	*/
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		return trip_id;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return taxi_id;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return trip_miles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return trip_total;
	}
	
	public int getDropoffComunityArea()
	{
		return dropoff_community_area;
	}

	@Override
	public int compareTo(Service o) 
	{
		return o.getDropoffComunityArea() == this.getDropoffComunityArea() ? 0 : -1;
	}
}
