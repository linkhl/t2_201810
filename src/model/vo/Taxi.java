package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	/**
	 * Id of the taxi.
	 */
	private String taxiId;
	
	/**
	 * company of the taxi.
	 */
	private String company;
	
	/**
	 * Constructor of the class taxi.
	 * @param pTaxiId Id of the taxi, given by the JSON.
	 * @param pCompany Name of the company of the taxi, given by the JSON 
	 */
	public Taxi(String pTaxiId, String pCompany)
	{
		taxiId = pTaxiId;
		company = pCompany;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() 
	{
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() 
	{
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		return o.getTaxiId().compareTo(this.getTaxiId());
	}
	
	@Override
	public String toString()
	{
		return taxiId;
		
	}
}
