package model.data_structures;

public class SimpleLinkedList <T extends Comparable<T>>implements ILinkedList<T>
{
	/**
	 * First Node of the List.
	 */
	private Node<T> firstNode;

	/**
	 * The current node of the iterator.
	 */
	private Node<T> currentNode;

	/**
	 * Quantity of elements in the List.
	 */
	private int size;


	/**
	 * Constructor of the simple Linked list.
	 * </b> Post: </b> The linked list has been initialized
	 */
	public SimpleLinkedList() 
	{
		firstNode = null;
		currentNode = null;
		size = 0;
	}

	@Override
	public int getSize()
	{
		return size;
	}

	/**
	 * Returns the first Node of the list.
	 * @return firstNode.
	 */
	public Node<T> getFirstNode()
	{
		return firstNode;
	}	

	@Override
	public void add(T pToAdd) 
	{
		if (firstNode == null)
		{
			firstNode = new Node<T>(pToAdd);
			size++;
		}
		else
		{
			firstNode.add(pToAdd);
			size++;
		}				
	}	

	@Override
	public void delete(T pToDelete) throws Exception
	{
		if(firstNode!=null)
		{
			listing();
			if(currentNode.getObject().compareTo(pToDelete) == 0)
				firstNode = firstNode.getNextNode();
			else if(currentNode.getNextNode().getObject().compareTo(pToDelete) == 0)
				currentNode.changeNextNode(currentNode.getNextNode().getNextNode());
			else
			{
				boolean deleted = false;
				while(next() && !deleted)
					if (currentNode.getNextNode()!= null? currentNode.getNextNode().getObject().compareTo(pToDelete) == 0:false)
					{
						currentNode.changeNextNode(currentNode.getNextNode().getNextNode());
						deleted = true;
					}
				if(!deleted)
					throw new Exception ("El elemento a eliminar no existia");
			}
			size --;
		}
	}

	public Node<T> getNode(int pPosition)
	{
		if(pPosition < 0 || pPosition > size)
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + pPosition + " y el tama�o de la lista es de " + size);

		int posActual = 0;
		Node<T> actual = firstNode;
		while(actual.getNextNode()!= null && posActual < pPosition)
		{
			actual =actual.getNextNode();
			posActual++;
		}
		return actual;
	}

	@Override
	public T get(int pPosition) 
	{
		if(pPosition < 0 || pPosition > size)
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + pPosition + " y el tama�o de la lista es de " + size);

		int posActual = 0;
		Node<T> actual = firstNode;
		while(actual.getNextNode()!= null && posActual < pPosition)
		{
			actual =actual.getNextNode();
			posActual++;
		}
		return actual.getObject();
	}

	@Override
	public T get(T pElement) 
	{
		T found = null;
		if (firstNode != null)
		{
			Node<T> actual = firstNode;
			if (actual.getObject().compareTo(pElement) == 0)
				found = actual.getObject();

			while(actual.getNextNode() != null && found != null)
			{

				if (actual.getObject().compareTo(pElement) == 0)
					found = actual.getObject();
				actual = actual.getNextNode();
			}
		}
		return found;
	}

	/**
	 * Method that checks if the T element given is alredy in the list.
	 * @param pElement element to be checked
	 * @return true if the List contains the given element, false instead.
	 */
	public boolean contains(T pElement)
	{
		boolean exist = false;
		if (firstNode != null)
		{
			Node<T> actual = firstNode;
			if (actual.getObject().compareTo(pElement) == 0)
				exist = true;
			while(actual.getNextNode() != null && !exist)
			{
				if (actual.getObject().compareTo(pElement) == 0)
					exist = true;
				actual = actual.getNextNode();
			}
		}
		return exist;
	}

	@Override
	public void listing() 
	{
		currentNode = firstNode;
	}

	@Override
	public Node<T> getCurrent() 
	{
		return currentNode;
	}

	@Override
	public boolean next() 
	{
		boolean next = false;
		if (firstNode != null)
		{
			if (currentNode.getNextNode()!= null)
			{
				currentNode = currentNode.getNextNode();
				next = true;
			}

		}
		return next;	

	}
}