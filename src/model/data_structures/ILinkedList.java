package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface ILinkedList <T extends Comparable<T>>  
{	
	/**
	 * Add a new element to the list.
	 * @param pToAdd element to be add. pToAdd != null.
	 */
	public void add(T pToAdd);
	
	/**
	 * Deletes the element given by the user.
	 * @param pToDelete Element to be deleted.
	 * @throws Exception If the element doesn't exist on the list.
	 */
	public void delete(T pToDelete) throws Exception;
	
	/**
	 * Gets the element that the user wanna use.
	 * @param pElement Element to search.
	 * @return The T element that was searched, null if it doesn't exists.
	 */
	public T get(T pElement);
	
	/**
	 * Gets the element T searched in the List.
	 * @param pPosition position of the object.
	 * @return Searched T object. 
	 */
	public T get(int pPosition);
	
	/**
	 * Gives the size of the list.
	 * @return size.
	 */
	public int getSize();
	
	/**
	 * Set the current Node to the firts node of the list.
	 */
	public void listing();
	
	/**
	 * Get's the current node of the iterator.
	 * @return current Node.
	 */
	public Node<T> getCurrent();
	
	/**
	 * Set the current node as the next in the list.
	 * @return true if there has been a change, instead false.
	 */
	public boolean next();
}
