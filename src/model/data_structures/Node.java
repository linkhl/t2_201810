package model.data_structures;

public class Node <T extends Comparable<T>> 
{
	/**
	 * Next node.
	 */
	protected Node<T> nextNode;
	
	/**
	 * Element in the node.
	 */
	protected T object;
	
	/**
	 * Constuctor of NODE
	 * @param pObject Object that will be saved in the node.
	 */
	public Node (T pObject)
	{
		nextNode = null;
		object = pObject;
	}
	
	/**
	 * Change the object that is in the node.
	 * @param pObject The new object of the node.
	 */
	public void changeObject(T pObject)
	{
		object = pObject;
	}
	
	/**
	 * Change the next Node of the actual Node.
	 * @param pNext New nextnode.
	 */
	public void changeNextNode(Node<T> pNext)
	{
		nextNode = pNext;
	}
	
	/**
	 * Method that returns the nextNode of the actual Node.
	 * @return nextNode.
	 */
	public Node<T> getNextNode()
	{
		return nextNode;
	}
	
	/**
	 * Method that returns the element in the node.
	 * @return object.
	 */
	public T getObject()
	{
		return object;
	}

	public void add(T pToAdd) 
	{
		if (nextNode == null)
			nextNode = new Node<T>(pToAdd);
		else
			nextNode.add(pToAdd);				
	}
}
