package model.data_structures;

public class DoubleLinkedList <T extends Comparable<T>> implements ILinkedList<T>
{
	/**
	 * First Node of the List.
	 */
	private DoubleNode<T> firstNode;

	/**
	 * 
	 */
	private DoubleNode<T> currentNode;

	/**
	 * Quantity of elements in the List.
	 */
	private int size;

	public DoubleLinkedList() 
	{
		firstNode = null;
		currentNode = null;
		size= 0;
	}

	@Override
	public void add(T pToAdd) 
	{
		DoubleNode<T> newNode = new DoubleNode<T>(pToAdd);
		while(next());	
		currentNode.changeNextNode(newNode);
		newNode.changePreviousNode(currentNode);
	}

	@Override
	public void delete(T pToDelete) throws Exception 
	{
		listing();
		boolean deleted = false;
		if(currentNode.getObject().compareTo(pToDelete) == 0)
		{
			firstNode = firstNode.getNextNode();
			firstNode.changePreviousNode(null); 
		}
		else
			while(next() && !deleted)
				if (currentNode.getObject().compareTo(pToDelete) == 0)
				{
					currentNode.changeNextNode(currentNode.getNextNode().getNextNode());
					currentNode.getNextNode().changePreviousNode(currentNode);
				}		
	}

	public T get(T pElement) 
	{
		listing();
		T found = null;
		while(next() && found == null)
			if (currentNode.getObject().compareTo(pElement) == 0)
				found = currentNode.getObject();
		return found;
	}


	public T get(int pPosition) 
	{
		if(pPosition < 0 || pPosition > size)
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + pPosition + " y el tama�o de la lista es de " + size);
		listing();
		int posActual = 0;
		while(next() && posActual < pPosition)
			posActual++;
		return currentNode.getObject();
	}


	public int getSize() 
	{
		return size;
	}

	@Override
	public void listing() 
	{
		currentNode = firstNode;
	}

	@Override
	public Node<T> getCurrent() 
	{
		return currentNode;
	}

	@Override
	public boolean next() 
	{
		boolean next = false;
		if (currentNode.getNextNode() != null)
		{
			next = true;
			currentNode = currentNode.getNextNode();	
		}
		return next;
	}

	public boolean previous()
	{
		boolean previous = false;
		if (currentNode.getPreviousNode() != null)
		{
			previous=true;
			currentNode = currentNode.getPreviousNode();
		}
		return previous;
	}

	/**
	 * Turn to an empty List.
	 */
	public void clear()
	{
		firstNode = null; 
		currentNode = null;
	}
}
