package model.data_structures;

import com.sun.xml.internal.ws.api.pipe.NextAction;

public class DoubleNode <T extends Comparable<T>> extends Node<T> 
{	
	/**
	 * Previous node.
	 */
	private DoubleNode<T> previousNode;

	/**
	 * Constructor of the Double Node.
	 * @param pObject element to be saved in the node.
	 */
	public DoubleNode(T pObject) 
	{
		super(pObject);
		previousNode = null;
	}
	
	/**
	 * Return previousNode
	 * @return previosNode.
	 */
	public DoubleNode<T> getPreviousNode()
	{
		return previousNode;
	}
	
	public DoubleNode<T> getNextNode()
	{
		return (DoubleNode<T>) nextNode;
	}
	
	public void changeNextNode(DoubleNode<T> pNext)
	{
		nextNode = pNext;
	}
	
	/**
	 * Change the value of previousNode, establishing the one that comes in the parameter.
	 * @param pPrevious DoubleNode that wanna be replaced.
	 */
	public void changePreviousNode(DoubleNode<T> pPrevious)
	{
		previousNode = pPrevious;
	}
}
