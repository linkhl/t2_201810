package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.ILinkedList;
import model.data_structures.SimpleLinkedList;
import model.vo.Service;
import model.vo.Taxi;
import sun.java2d.pipe.SpanShapeRenderer.Simple;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	/**
	 * List of taxis.
	 */
	private SimpleLinkedList<Taxi> taxis;

	/**
	 * List of services.
	 */
	private SimpleLinkedList<Service> services;

	public void loadServices (String serviceFile)
	{
		taxis = new SimpleLinkedList<Taxi>();
		services = new SimpleLinkedList<Service>();
		JsonParser parser = new JsonParser();
		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";

			JsonArray jsonObjectList= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));

			for (int i = 0; jsonObjectList != null && i < jsonObjectList.size(); i++)
			{
				JsonObject obj= (JsonObject) jsonObjectList.get(i);
				System.out.println("------------------------------------------------------------------------------------------------");
				System.out.println(obj);
				//------------------------------------------------------------
				// Primero creo el taxi.
				//------------------------------------------------------------
				String company = "NaN";
				if (obj.get("company") != null)
					company = obj.get("company").getAsString();
				String taxi_id = "NaN";
				if (obj.get("taxi_id") != null)
					taxi_id = obj.get("taxi_id").getAsString();
				Taxi newTaxi = new Taxi(taxi_id, company);
				if (!taxis.contains(newTaxi))
					taxis.add(newTaxi);
				//------------------------------------------------------------
				// Ya termine de crear el taxi. Ahora los servicios.
				//------------------------------------------------------------
				short dropoff_community_area = 0;
				if (obj.get("dropoff_community_area") !=null)
					dropoff_community_area = obj.get("dropoff_community_area").getAsShort();
				String trip_id = "";
				if (obj.get("trip_id") !=null)
					trip_id = obj.get("trip_id").getAsString();
				int trip_seconds = 0;
				if (obj.get("trip_seconds") !=null)
					trip_seconds = obj.get("trip_seconds").getAsInt();
				double trip_miles = 0;
				if (obj.get("trip_miles") !=null)
					trip_miles = obj.get("trip_miles").getAsDouble();
				double trip_total = 0;
				if (obj.get("trip_total") !=null)
					trip_total = obj.get("trip_total").getAsDouble();
				
				Service newService = new Service(dropoff_community_area, taxi_id, trip_id, trip_seconds, trip_miles, trip_total);
				services.add(newService);
			}

		}
		catch (JsonIOException e1 ) 
		{			
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2)
		{
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3)
		{
			e3.printStackTrace();
		}

	}

	@Override
	public ILinkedList<Taxi> getTaxisOfCompany(String company) 
	{
		SimpleLinkedList<Taxi> answer = new SimpleLinkedList<Taxi>();
		String[] c = company.split("_");
		company = ""; 
		for (int i = 0; i<c.length ; i++)
		{
			String actual = c[i];
			if(i==(c.length-1))
				company += actual;
			else
				company += actual + " ";		
			}
		for (int i = 0; i< taxis.getSize(); i++)
		{
			Taxi toCompare = taxis.get(i);
			if (toCompare.getCompany().equals(company) 
					&& !answer.contains(toCompare)	 )
				answer.add(toCompare);
		}
		System.out.println("Inside getTaxisOfCompany with " + company);		
		return answer;
	}

	@Override
	public ILinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) 
	{
		SimpleLinkedList<Service> answer = new SimpleLinkedList<Service>();
		for (int i = 0; i< services.getSize(); i++)
		{
			Service toCompare = services.get(i);
			if (toCompare.getDropoffComunityArea() == communityArea)
				answer.add(toCompare);
		}
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return answer;
	}
	
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------
	// De aqu� para abajo es codigo util para el proyecto, pero no para este Taller,
	// No need to look down here.
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**double dropoff_census_tract;
	if (obj.get("dropoff_census_tract")!= null)
		dropoff_census_tract = obj.get("dropoff_census_tract").getAsDouble();
	
	String dropoff_centroid_latitude = "NaN";
	if ( obj.get("dropoff_centroid_latitude") != null )
		dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString();


	JsonObject dropoff_centroid_localization_obj = null;
	// Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio
	if ( obj.get("dropoff_centroid_location") != null )
	{
		dropoff_centroid_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

		// Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)
		JsonArray dropoff_localization_arr = dropoff_centroid_localization_obj.get("coordinates").getAsJsonArray();

		// Obtener cada coordenada del JsonArray como Double 
		double longitude = dropoff_localization_arr.get(0).getAsDouble();
		double latitude = dropoff_localization_arr.get(1).getAsDouble();
	}
	else
	{
		System.out.println( "[Lon: NaN, Lat: NaN ]");
	}

	String dropoff_centroid_longitude = "NaN";
	if ( obj.get("dropoff_centroid_longitude") != null )
		dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString();
	*/
	
	// double dropoff_centroid_latitude - Object dropoff_centroid_location 
	// double dropoff_centroid_longitude - short dropoff_community_area 
	// double extras - double fare - String payment_type 
	// double pickup_census_tract - double pickup_centroid_latitude - Object pickup_centroid_location
	// double pickup_centroid_longitude - short pickup_community_area 
	// String taxi_id - int tips - int tolls
	// Date trip_end_timestamp - String trip_id - int trip_seconds - double trip_miles 
	// Date trip_start_timestamp - double trip_total
}
